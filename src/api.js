myDefine('api', ['HttpRequest'], HttpRequest => {
  const RESPONSE_TYPES = {
    BLOB: 'blob'
  };
  const myFetch = new HttpRequest({
    baseUrl: 'http://localhost:8000',
    interceptor: req => {
      if (req.status > 400) {
        req.onerror();
      }

      return req;
    }
  });
  const uploadFile = (data, config) => myFetch.post('/upload/', data, config);
  const downloadFile = (data, config) => myFetch.get(`/files/${data}`,
    { ...config, responseType: RESPONSE_TYPES.BLOB });
  const deleteFile = data => myFetch.delete(`/list/${data}`, { responseType: RESPONSE_TYPES.BLOB });
  const getList = () => myFetch.get('/list/');


  return { uploadFile, downloadFile, deleteFile, getList };
});
