myDefine([], () => {
  const STATUS = {
    SUCCESS: 'success',
    WARN: 'warn',
    ERROR: 'error'
  };


  class Notification extends HTMLElement {
    #messageWindow = null;
    #notification = null;

    #render = () => {
      this.#notification.innerHTML = `
      <style>
      :host {
        --success-bg-color: #38b13dd9;
        --error-bg-color: #ef0303b5;
        --warn-bg-color: #ad4d3cd9;
      }
      .wrapper {
        align-items: center;
        background: #e6f3f9;
        border-radius: 10px;
        color: #000;
        font-weight: bold; 
        display: flex;
        height: 50px;
        justify-content: center;
        opacity: 0;
        position: absolute;
        right: 20px;
        top: 10px;
        transition: all 0.5s ease-in-out;
        width: 200px;
      }

      </style>
      <div class="wrapper">
      <p></p>
      </div>`;
    }

    #notify = status => {
      let bgColor = '--warn-bg-color';

      if (status === STATUS.SUCCESS) {
        bgColor = '--success-bg-color';
      } else if (status === STATUS.ERROR) {
        bgColor = '--error-bg-color';
      }

      this.#messageWindow.style.cssText += `
        background: var(${bgColor}); 
        opacity: 1;
      `;
      setTimeout(() => {
        this.#messageWindow.style.opacity = 0;
      }, 1000);
    }

    static get observedAttributes() {
      return ['data-status', 'data-message'];
    }

    constructor() {
      super();
      this.#notification = this.attachShadow({ mode: 'closed' });
    }

    connectedCallback() {
      this.#render();
      this.#messageWindow = this.#notification.querySelector('.wrapper');
    }

    attributeChangedCallback(name, oldValue, newValue) {
      if (name === 'data-message') {
        this.#messageWindow.innerText = newValue;
        return;
      }
      this.#notify(newValue);
    }
  }

  customElements.define('custom-notification', Notification);
});
