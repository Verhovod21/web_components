myDefine(['api', 'utils'], ({ uploadFile }, { createCustomEvent }) => {
  class UploadForm extends HTMLElement {
    #uploadForm = null;
    #uploadBar = null;
    #inputTextValue = null;
    #form = null;

    #submitForm = e => {
      e.preventDefault();

      if (this.#inputTextValue.innerText === 'Select a file') {
        createCustomEvent(this, 'submitEmptyForm', { message: 'Select a file' });
        return;
      }

      const form = new FormData();
      const { sampleFile } = e.target;


      form.append(sampleFile.name, sampleFile.files[0]);
      uploadFile(form, {
        onUploadProgress: e => {
          const percent = Math.floor(e.loaded * 100 / e.total);


          this.#form.style.display = 'none';
          this.#uploadBar.dataset.percent = percent;

          if (percent === 100) {
            setTimeout(() => {
              this.#form.style.display = 'flex';
            }, 700);
          }
        }
      })
        .then(res => createCustomEvent(this, 'mySubmit', { data: res.data }))
        .catch(req => createCustomEvent(this, 'myError', { message: `${req.status} error: ${req.statusText}` }));
      this.#inputTextValue.innerText = 'Select a file';
    }

    #addInternalListener = () => {
      this.#uploadForm.querySelector('.input').onchange = this.#showSelectedFileName;
      this.#uploadForm.querySelector('.form').onsubmit = this.#submitForm;
    }

    #showSelectedFileName = e => {
      const fileAddressElements = e.target.value.split('\\');


      this.#inputTextValue.setAttribute('data-title', e.target.value);
      this.#inputTextValue.innerText = fileAddressElements[fileAddressElements.length - 1];
    }

    #render = () => {
      this.#uploadForm.innerHTML = `
        <style> 
        :host {
          align-items: center;
          display: flex;
          justify-content: space-around;
          width: 90%;
        }
        .uploadForm {
          width: 90%;
        }
        .form {
          align-items: center;
          display: flex;
          justify-content: space-around;
          width: 100%;
        }
        .input_wrapper {
          background: #e6f3f9;
          border-radius: 10px;
          color: #5B9BA2;
          display: inline-block;
          height: 40px;
          position: relative;
          overflow: hidden;
          text-align: center;
          width: 200px;
        }
        .input {
          display: none;
        }
        .file {
          font-weight: bold; 
          line-height: 40px;
          padding: 0 4px;
        }
        .file:hover::after {
          background: #e6f3f98c;
          border-radius: 0 2px 0 0;
          box-sizing: content-box;
          content: attr(data-title); 
          font-weight: normal;
          height: 20px;
          left: 0px; bottom: 0%;
          line-height: 20px;
          max-width: 400px;
          position: fixed; 
          z-index: 30; 
        }
        .btn_wrapper {
          background: url('./image/upload.svg') no-repeat;
          background-size: 100%;
          border-radius: 50%;
          cursor: pointer;
          height: 40px;
          position: relative;
          overflow: hidden;
          width: 40px;
        }
        .btn_wrapper label {
          cursor: pointer;
          display: block;
          top: 0;
          left: 0;
          height: 100%;
          position: absolute;
          width: 100%;
        }
        .btn_wrapper._download {
          background: url('./image/download.svg') no-repeat;
          background-size: 100%;
        }
        .input_wrapper:hover, .btn_wrapper:hover{
          box-shadow: 0px 0px 32px -3px rgba(11, 68, 112, 0.47);
        }
        .btn {
          display: none;
        }

        </style>
        
        <form class="form" encType="multipart/form-data">
        <div class="input_wrapper">
          <label>
            <input type="file" name="sampleFile" class="input"/>
            <span class="file">Select a file</span>
          </label>
        </div>
        <div class="btn_wrapper">
          <label>
            <input type="submit" value="Upload!" class="btn"/>
          </label>
        </div>
      </form>`;
    }

    constructor() {
      super();
      this.#uploadForm = this.attachShadow({ mode: 'closed' });
    }

    connectedCallback() {
      this.#render();
      this.#inputTextValue = this.#uploadForm.querySelector('.file');
      this.#addInternalListener();
      this.#uploadBar = document.querySelector('.upload_bar');
      this.#form = this.#uploadForm.querySelector('.form');
    }
  }

  customElements.define('upload-form', UploadForm);
});
