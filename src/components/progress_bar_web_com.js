myDefine([], () => {
  class ProgressBar extends HTMLElement {
    #progressBar = null;
    #graphicIndicationProgress = null;
    #percentIndicationProgress = null;

    #render = () => {
      this.#progressBar.innerHTML += this.getAttribute('showpercent') ? `
      <style>
      :host {
        align-items: center;
        display: flex;
        display: none;
        justify-content: space-around;
        margin-bottom: 16px;
        width: 90%;
        z-index: 20;
      }
      .progress_bar {
        align-items: center;
        background: #364076;
        border-radius: 10px;
        justify-content: space-around;
        margin-bottom: 16px;
        height: 100%;
        transition: width 0.18s ease-in-out;
        width: 90%;
        width: 20%;
      }
      .wrapper {
        background: #e6f3f9;
        border-radius: 10px;
        color: #5B9BA2;
        display: inline-block;
        height: 40px;
        text-align: center;
        width: 200px;
      }
      .persent_indicator {
        background: white;
        border-radius: 50%;
        box-sizing: border-box;
        height: 40px;
        color: #5B9BA2;
        font-weight: bold; 
        margin: 0;
        padding: 12px 0;
        text-align: center;
        width: 40px;
      }
      </style>
        <div class="wrapper">
          <div class="progress_bar"></div>
        </div>
        <p class="persent_indicator">0%</p>   
        ` : `
        <style>
      :host {
        height: 3px;
        position: absolute;
        top: 0;
        width: 100%;
        display: none;
      }
        .progress_bar {
          background: #364076;
          border-radius: 10px;
          height: 3px;
          top: 0;
          transition: width 0.18s ease-in-out;
          position: absolute;
          width: 100%;
          width: 0%;
        }
      </style>
        <div class="progress_bar"></div>`;
    }

    static get observedAttributes() {
      return ['data-percent'];
    }

    constructor() {
      super();
      this.#progressBar = this.attachShadow({ mode: 'closed' });
    }

    connectedCallback() {
      this.#render();
      this.#graphicIndicationProgress = this.#progressBar.querySelector('.progress_bar');
      this.#percentIndicationProgress = this.#progressBar.querySelector('.persent_indicator');
    }

    attributeChangedCallback(name, oldValue, newValue) {
      this.style.display = 'flex';
      this.#graphicIndicationProgress.style.width = `${newValue}%`;

      if (this.#percentIndicationProgress) {
        this.#percentIndicationProgress.textContent = `${newValue}%`;
      }

      if (Number(newValue) === 100) {
        setTimeout(() => {
          this.style.display = 'none';
        }, 700);
      }
    }
  }

  customElements.define('progress-bar', ProgressBar);
});
