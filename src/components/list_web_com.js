myDefine(['api', 'utils'], ({ getList, deleteFile }, { createCustomEvent }) => {
  class List extends HTMLElement {
    #listWrapper = null;
    #list = null;
    #listModel = new Proxy([], {
      set: (target, property, value) => {
        target[property] = value;
        this.#updateListView();
        return true;
      }
    });

    #updateListView = () => {
      const fragment = document.createDocumentFragment();


      this.#list.innerHTML = '';
      this.#listModel.forEach(elem => {
        fragment.appendChild(this.#createCell(elem.name));
      });
      this.#list.appendChild(fragment);
    }

    #updateDataModel = () => {
      getList().then(({ data }) => {
        this.#listModel.length = 0;
        this.#listModel.push(...data);
      });
    }

    #domRangeCreate = value => {
      this.#updateListView();
      const allCell = this.#listWrapper.querySelectorAll('.cell_name');


      allCell.forEach(cell => {
        const elem = cell.firstChild;
        const content = elem.nodeValue;
        const index = content.indexOf(value);


        if (index < 0) {
          return;
        }

        const rng = document.createRange();
        const highlightDiv = document.createElement('span');


        rng.setStart(elem, content.indexOf(value));
        rng.setEnd(elem, content.indexOf(value) + value.length);
        highlightDiv.style.backgroundColor = '#7c7cc1';
        rng.surroundContents(highlightDiv);
      });
    };

    #onItemClick = (e, value) => {
      e.preventDefault();
      const selectedItem = this.#listModel.filter(item => item.name === value)[0];


      if (e.target.className === 'remove_cell') {
        this.#listModel.splice(this.#listModel.indexOf(selectedItem), 1);
        deleteFile(selectedItem.name);
        return;
      }
      createCustomEvent(this, 'selectedElem', { name: selectedItem.name });
    };

    #createCell = value => {
      const li = document.createElement('li');


      li.className = 'cell';
      li.dataset.name = value;
      li.onclick = e => {
        this.#onItemClick(e, value);
      };
      li.innerHTML = `
    <span class ="cell_name">${value}</span>
    <button type="button" class="remove_cell" value="${value}"></button>`;
      return li;
    }

    #render = () => {
      this.#listWrapper.innerHTML = `
      <style>
      .list {
        padding: 0;
      }
      .cell {
        display: flex;
        list-style-type: none;
        justify-content: space-between;
        border-radius: 2px;
        padding: 3px 4px;
        width: 100%;
      }
      .cell_name {
        cursor: pointer;
        width: 95%;
      }
      .remove_cell {
        background: url('./image/remove.svg');
        border: none;
        cursor: pointer;
        height: 20px;
        opacity: 0.5;
        width: 20px;
      }
      .cell:hover {
        box-shadow: 2px 4px 5px 2px rgba(48, 117, 163, 0.18) inset;
      
      } 
      .remove_cell:hover {
        box-shadow: 0px 0px 5px 6px rgba(240, 62, 62, 0.2);
        opacity: 1;
      }
      </style>
      <ul class="list"></ul>`;
    };

    static get observedAttributes() {
      return ['data-last-updated', 'data-highlighted-symbols'];
    }

    constructor() {
      super();
      this.#listWrapper = this.attachShadow({ mode: 'closed' });
    }

    connectedCallback() {
      this.#updateDataModel();
      this.#render();
      this.#list = this.#listWrapper.querySelector('.list');
      this.#updateListView();
    }

    attributeChangedCallback(name, oldValue, newValue) {
      if (name === 'data-last-updated') {
        this.#updateDataModel();
      }

      if (name === 'data-highlighted-symbols') {
        this.#domRangeCreate(newValue);
      }
    }
  }

  customElements.define('my-list', List);
});
