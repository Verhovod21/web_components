myDefine([], () => {
  class Picture extends HTMLElement {
    #pictureWrapper = null;
    #img = null;

    #render = () => {
      this.#pictureWrapper.innerHTML = `
    <style>
    .picture {
      max-width: 400px;
      border-radius: 4px;
    }
    </style>
    <img class="picture">`;
    }

    static get observedAttributes() {
      return ['data-url'];
    }

    constructor() {
      super();
      this.#pictureWrapper = this.attachShadow({ mode: 'closed' });
    }

    connectedCallback() {
      this.#render();
      this.#img = this.#pictureWrapper.querySelector('.picture');
    }

    attributeChangedCallback(name, oldValue, newValue) {
      this.#img.src = newValue;
    }
  }

  customElements.define('main-picture', Picture);
});
