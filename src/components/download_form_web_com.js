myDefine(['api', 'utils'], ({ downloadFile }, { createCustomEvent }) => {
  class DownloadForm extends HTMLElement {
    #form = null;
    #inputText = null;

    #submitForm = e => {
      e.preventDefault();
      const input = e.target.sampleFile;
      const name = input.value;


      if (!name) {
        createCustomEvent(this, 'submitEmptyForm', { message: 'Select a file' });
        return;
      }

      downloadFile(name, {
        onDownloadProgress: e => {
          ('loadProgress', { percent: Math.floor(e.loaded * 100 / e.total) });
        }
      }).then(res => createCustomEvent(this, 'mySubmit', {
        data: res.data,
        name
      }))
        .catch(req => createCustomEvent(this, 'myError', { message: `${req.status} error: ${req.statusText}` }));
    };

    #setInternalListener = () => {
      this.#inputText.oninput = e => createCustomEvent(this, 'changeInputValue', { value: e.target.value });
      this.#form.querySelector('.form').onsubmit = this.#submitForm;
    }

    #render = () => {
      this.#form.innerHTML = `
        <style>
        :host {
          align-items: center;
          display: flex;
          justify-content: space-around;
          width: 100%;
        }
        .wrapper {
          width: 90%;
        }
        .form {
          // align-items: center;
          display: flex;
          justify-content: space-around;
          width: 100%;
        }
        .input_text_wrapper {
          background: #e6f3f9;
          border-radius: 10px;
          color: #5B9BA2;
          display: inline-block;
          height: 40px;
          position: relative;
          overflow: hidden;
          text-align: center;
          width: 200px;
        }

        .input_text_wrapper label {
          cursor: pointer;
          display: block;
          font-size: 14px;
          top: 0;
          left: 0;
          height: 100%;
          position: absolute;
          overflow: hidden;
          text-overflow: ellipsis;
          width: 100%;
          white-space: nowrap;
        }

        .input_text_wrapper:hover, .input_submit_wrapper:hover{
          box-shadow: 0px 0px 32px -3px rgba(11, 68, 112, 0.47);
        }

        .input_text {
          background-color: #e6f3f9;
          border: none;
          color: #5B9BA2;
          font-weight: bold; 
          font-family: inherit;
          height: 100%;
          outline: 0;
          width: 90%;
        }

        .input_submit_wrapper {
          background: url('./image/upload.svg') no-repeat;
          background-size: 100%;
          border-radius: 50%;
          cursor: pointer;
          height: 40px;
          position: relative;
          overflow: hidden;
          width: 40px;
        }
        .input_submit_wrapper label {
          cursor: pointer;
          display: block;
          top: 0;
          left: 0;
          height: 100%;
          position: absolute;
          width: 100%;
        }
        .input_submit_wrapper {
          background: url('./image/download.svg') no-repeat;
          background-size: 100%;
        }
        .submit_btn {
          display: none;
        }

        </style>
        <div class ="wrapper">
          <form class="form">
          <div class="input_text_wrapper">
            <label>
              <input type="text" name="sampleFile" class="input_text" />
            </label>
          </div>
          <div class="input_submit_wrapper">
            <label>
              <input type='submit' value='Download!' class="submit_btn" />
            </label>
          </div>
        </form>
      </div>
     `;
    }

    static get observedAttributes() {
      return ['data-value'];
    }

    constructor() {
      super();
      this.#form = this.attachShadow({ mode: 'closed' });
    }

    attributeChangedCallback(name, oldValue, newValue) {
      this.#inputText.value = newValue;
    }

    connectedCallback() {
      this.#render();
      this.#inputText = this.#form.querySelector('.input_text');
      this.#setInternalListener();
    }
  }

  customElements.define('download-form', DownloadForm);
});
