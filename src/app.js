myDefine(['utils'], ({ downloadData }) => {
  const IMAGE_BLOB_TYPES = ['image/gif', 'image/jpeg', 'image/png'];
  const uploadForm = document.querySelector('upload-form');
  const downloadForm = document.querySelector('download-form');
  const downloadBar = document.querySelector('progress-bar');
  const list = document.querySelector('my-list');
  const notification = document.querySelector('custom-notification');
  const image = document.querySelector('main-picture');


  downloadForm.addEventListener('mySubmit', ({ detail }) => {
    const { data, name } = detail;
    const { type } = data;
    const url = URL.createObjectURL(data);


    if (IMAGE_BLOB_TYPES.some(imageBlobType => imageBlobType === type)) {
      image.dataset.url = url;
      return;
    }

    downloadData(url, name);
  });
  downloadForm.addEventListener('mySubmit', () => {
    notification.dataset.message = 'Successfully completed';
    notification.dataset.status = 'success';
  });
  downloadForm.addEventListener('submitEmptyForm', ({ detail }) => {
    notification.dataset.message = detail.message;
    notification.dataset.status = 'warn';
  });
  downloadForm.addEventListener('myError', ({ detail }) => {
    notification.dataset.message = detail.message;
    notification.dataset.status = 'error';
  });
  downloadForm.addEventListener('changeInputValue', ({ detail }) => {
    list.dataset.highlightedSymbols = detail.value;
  });
  downloadForm.addEventListener('loadProgress', ({ detail }) => {
    downloadBar.dataset.percent = detail.percent;
  });
  uploadForm.addEventListener('submitEmptyForm', ({ detail }) => {
    notification.dataset.message = detail.message;
    notification.dataset.status = 'warn';
  });
  uploadForm.addEventListener('error', ({ detail }) => {
    notification.dataset.message = detail.message;
    notification.dataset.status = 'error';
  });
  uploadForm.addEventListener('mySubmit', () => {
    notification.dataset.message = 'Successfully completed';
    notification.dataset.status = 'success';
  });
  uploadForm.addEventListener('mySubmit', () => {
    list.dataset.lastUpdated = Date.now();
  });
  list.addEventListener('selectedElem', ({ detail }) => {
    downloadForm.dataset.value = detail.name;
  });
});
