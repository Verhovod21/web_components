myDefine('utils', [], () => {
  const downloadData = (url, name) => {
    const urlElem = document.createElement('a');
    urlElem.href = url;
    urlElem.download = name;
    urlElem.click();
    urlElem.remove();
  };

  const createCustomEvent = function(context, name, detail) {
    context.dispatchEvent(new CustomEvent(name, {
      composed: true,
      detail
    }));
  };

  return { downloadData, createCustomEvent };
});
