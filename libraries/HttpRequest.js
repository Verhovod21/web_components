myDefine('HttpRequest', [], () => {
  const RESPONCE_TYPE = {
    JSON: 'json',
    BLOB: 'blob'
  };
  const REQUEST_METHODS = {
    DELETE: 'DELETE',
    GET: 'GET',
    PATCH: 'PATCH',
    POST: 'POST',
    PUT: 'PUT'
  };


  class HttpRequest {
    constructor({ baseUrl, headers, interceptor }) {
      this.baseUrl = baseUrl;
      this.headers = headers;
      this.interceptor = interceptor;
    }

    get(url, config) {
      return this.request({
        ...config,
        method: REQUEST_METHODS.GET,
        url
      });
    }

    post(url, data, config) {
      return this.request({
        ...config,
        data,
        method: REQUEST_METHODS.POST,
        url
      });
    }

    delete(url, config) {
      return this.request({
        ...config,
        method: REQUEST_METHODS.DELETE,
        url
      });
    }

    request(config) {
      const fullUrl = new URL(config.url, this.baseUrl);
      const headers = Object.assign({}, this.headers, config.headers);
      const req = new XMLHttpRequest();


      fullUrl.search = new URLSearchParams(config.params);
      req.responseType = config.responseType || RESPONCE_TYPE.JSON;

      if (config.onDownloadProgress) {
        req.onprogress = config.onDownloadProgress;
      }

      if (config.onUploadProgress) {
        req.upload.onprogress = config.onUploadProgress;
      }

      req.open(config.method, fullUrl.href);

      for (const key in headers) {
        req.setRequestHeader(key, headers[key]);
      }
      req.send(config.data);
      return new Promise((res, rej) => {
        req.onload = () => {
          const requestData = this.interceptor ? this.interceptor(req) : req;
          const responseData = config.transformResponse && req.status >= 200 && req.status <= 300 ?
            config.transformResponse.reduce((data, fn) => fn(data), requestData.response) :
            requestData.response;


          res({
            url: fullUrl.href,
            status: requestData.status,
            headers,
            data: responseData
          });
        };
        req.onerror = () => {
          rej(req);
        };
      });
    }
  }
  return HttpRequest;
});
