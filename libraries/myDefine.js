(() => {
  const dependenses = {};
  function myDefine() {
    let moduleName = null;
    let dependens = null;
    let fn = null;

    if (arguments.length === 3) {
      moduleName = arguments[0];
      dependens = arguments[1];
      fn = arguments[2];
    } else {
      moduleName = '';
      dependens = arguments[0];
      fn = arguments[1];
    }

    let args = null;
    const arg = Promise.all(dependens);
    arg.then(a => {
      args = dependens.map(dep => dependenses[dep]);
      return fn(...args);
    }).then(res => {
      dependenses[moduleName] = res;
    });
  }
  myDefine.cashe = dependenses;
  window.myDefine = myDefine;
})();
