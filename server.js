/* eslint-disable no-sync */
const express = require('express');
const fs = require('fs');
const fileUpload = require('express-fileupload');
const app = express();
const path = require('path');
const cors = require('cors');
const getFileInfoFromFolder = route => {
  const files = fs.readdirSync(route, 'utf8');
  const response = [];


  for (const file of files) {
    if (file !== '.gitignore') {
      const extension = path.extname(file);
      const fileSizeInBytes = fs.statSync(`${__dirname}/uploads/${file}`).size;
      response.push({ name: file, extension, fileSizeInBytes });
    }
  }
  return response;
};


app.use('/form', express.static(path.join(__dirname, '/index.html')));
app.use('/js', express.static(path.join(__dirname, '/src')));
app.use('/lib', express.static(path.join(__dirname, '/libraries')));
app.use('/files', express.static(path.join(__dirname, '/uploads')));
app.use('/style', express.static(path.join(__dirname, '/style.css')));
app.use('/image', express.static(path.join(__dirname, '/image')));
app.use('/list', express.static(path.join(__dirname, '/uploads')));

// default options
app.use(cors());
app.use(fileUpload());
app.get('/list', function(req, res) {
  res.send(getFileInfoFromFolder(`${__dirname}/uploads`));
});
app.delete('/list/:name', function(req, res) {
  const encodeUrl = req.params.name;
  fs.unlink(`${__dirname}/uploads/${encodeUrl}`, error => {
    if (error) {
      return Error(error);
    }
    res.send(`Got a DELETE request at /${encodeUrl}`);
  });
});
app.post('/ping', function(req, res) {
  res.send('pong');
});
app.post('/upload', function(req, res) {
  let sampleFile = null;
  let uploadPath = null;

  if (Object.keys(req.files).length === 0) {
    res.status(400).send('No files were uploaded.');
    return;
  }

  sampleFile = req.files.sampleFile; // eslint-disable-line
  uploadPath = path.join(__dirname, '/uploads/', sampleFile.name);
  sampleFile.mv(uploadPath, function(err) {
    if (err) {
      return res.status(500).send(err);
    }

    res.send(path.join('File uploaded to ', uploadPath));
  });
});

app.listen(8000, function() {
  console.log('Express server listening on port 8000'); // eslint-disable-line
});
